
# Gene Allen Bruce Jr [^disclaimer]

Birthday: 1/26/1959

Height/Weight: 6'0" / 340 LBS

Age: 62

Reason: Commission

---

## Addresses

* Addresses:
    * 4211 Swan St., Hitchcock, TX 77563 (Current)
    * 7501 Western Dr., Hitchcock, TX 77563
    * 1611 Newcomb Way, Houston, TX, 77058
    * 329 11th Ave. N, Texas City, TX 77590

* Phone Numbers:
    * (409) 739-5800 (Verizon Wireless, Galveston TX)
        * Cell Phone (90%)
        * Voice: <https://voca.ro/1lVU4Au5PiT0>
    * (956) 235-3960 (Sprint, Laredo TX)
    * (409) 935-3160 (AT&T Southwest, Texas City TX)

* Email Address: [^email]
    * gene.bruce@att.net
    * genebruce@netzero.net
    * brucegene4@gmail.com

---

## Liens And Judgements

* Record 1
    * DEBTOR: Gene Bruce
    * DEBTOR ADDRESS: 4211 Swan St. Hitchcock, TX 77563
    * CREDITOR: Robert J Roth
    * AMOUNT: $10,000
    * FILING DATE: November 15, 2010
    * FILING JURISDICTION: TX
    * FILING TYPE: JUDGEMENT LIEN
    * FILING AGENCY: GALVESTION COUNTY CLERK-GALVESTON
    * FILING AGENCY STATE: TX
    * FILING NUMBER: 2010056888
---
* Record 2
    * DEBTOR: Gene Bruce
    * DEBTOR ADDRESS: 4211 Swan St. Hitchcock, TX 77563
    * CREDITOR: ROBERT J ROTH
    * EVICTION: N
    * AMOUNT: $10,000
    * FILING DATE: January 6, 2010
    * FILING JURISDICTION: TX
    * FILING TYPE: SMALL CLAIMS JUDGMENT
    * FILING AGENCY: GALVESTON JP CT 1-2 - GALVESTON
    * FILING AGENCY STATE: TX
    * FILING NUMBER: S01090078
---
* Record 3
    * DEBTOR: Gene Bruce
    * DEBTOR ADDRESS: 4211 Swan St. Hitchcock, TX 77563
    * CREDITOR: BONIFACIO FILOTEO
    * CREDITOR COMPANY: BONIFACIO FILOTEO
    * EVICTION: N
    * AMOUNT: $500
    * FILING DATE: July 24, 2007
    * FILING JURISDICTION: TX
    * FILING TYPE: SMALL CLAIMS JUDGMENT
    * FILING AGENCY: GALVESTON JP CT 4 - SANTA FE
    * FILING AGENCY STATE: TX
    * FILING NUMBER: SC04070031
---
* Record 4
    * DEBTOR: Gene Bruce
    * DEBTOR ADDRESS: 4211 Swan St. Hitchcock, TX 77563
    * CREDITOR: ANGELA M THOMAS
    * EVICTION: N
    * AMOUNT: $212
    * FILING DATE: July 20, 2004
    * FILING JURISDICTION: TX
    * FILING TYPE: CIVIL JUDGMENT
    * FILING AGENCY: GALVESTON JP CT 4 - SANTA FE
    * FILING AGENCY STATE: TX
    * FILING NUMBER: J0006419
---
* Misc. Records
    * <https://www.judyrecords.com/record/22qtflra66d>
    * <https://www.judyrecords.com/record/22sfsz57ba0>
    * <https://www.judyrecords.com/record/c19ffmm19eb>
    * <https://www.judyrecords.com/record/c1wisbr9214>
    * <https://www.judyrecords.com/record/c1br3kg6b2d>
    * <https://www.judyrecords.com/record/c1b92d5774c>
    * For more, just look up the address on judyrecords.com

---

## Interests [^interests]

* GENERAL INTERESTS
    * Home Decor
    * Gourmet Cooking
    * Gardening
    * Photography
    * Toys

* MUSIC INTERESTS
    * Entertainment

* SPORTS INTERESTS
    * Hunting
  
* FITNESS INTERESTS
    * Active Life
    * Active Outdoors

* AUTOMOTIVE INTERESTS
    * Auto Work
    * Motorcycle Owner
    * Truck Owner
  
* COLLECTORS INTERESTS
    * Coins
    * Stamps
  
* HOUSEHOLD INTERESTS
    * 1 Generation
    * 2 Adults In Home
    * 2 People In Home

* LIFESTYLE INTERESTS[^lifestyle]
    * ~$159k TX Home Value
    * ~$50k-$60k Income
    * Has Investments
    * Some College
    * Likes Traveling
    * English Language

## Family

* Kelly M Mason, 58
    * Divorced on 04/19/1993
    * 306TH DISTRICT COURT
* Alice A Bruce, 87
* John M Bruce, 61
* Stacy Bruce

[^disclaimer]: All Information comes from public data sources which may be inaccurate, out-of-date or otherwise wrong. Use of this is at your own risk. We make no guarantees, expressed or implied, as to the accuracy of this data or service.
[^interests]: Interest Information based on household and collected by companies and events for marketing usage.
[^lifestyle]: Here are the sources we collect and search to populate data for this section: Owned Properties, Social Profiles, Business Records, and Lifestyle Marketing Data
[^email]: Tend to be the most unreliable
